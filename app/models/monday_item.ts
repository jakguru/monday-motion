import { DateTime } from 'luxon'
import { BaseModel, column } from '@adonisjs/lucid/orm'

/**
 * Model which represents a Monday item
 * See @link https://developer.monday.com/api-reference/reference/items for more information
 */

export default class MondayItem extends BaseModel {
  @column({ isPrimary: true })
  declare id: number

  @column()
  declare mondayBoardId: number

  @column()
  declare mondayCreatorId: number

  @column()
  declare mondayItemId: number

  @column()
  declare mondayItemsParentId: number | null

  @column()
  declare mondayEmail: string

  @column()
  declare mondayName: string

  @column()
  declare mondayState: string

  @column.dateTime()
  declare mondayCreatedAt: DateTime

  @column.dateTime({ autoCreate: true })
  declare createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  declare updatedAt: DateTime
}
