import type { ApplicationService } from '@adonisjs/core/types'
import { OpenAPIClientAxios } from 'openapi-client-axios'
import { MotionOpenApiDefinition } from '../types/motion.js'
import type { Client as MotionApiClient } from '../types/motion.js'
import { server, token } from '#config/motion'

declare module '@adonisjs/core/types' {
  interface ContainerBindings {
    motion: MotionApiClient
  }
}

export default class MotionProvider {
  private client: MotionApiClient
  constructor(protected app: ApplicationService) {
    const api = new OpenAPIClientAxios({
      definition: MotionOpenApiDefinition,
      quick: false,
      withServer: {
        url: server,
        description: 'DocFox API',
      },
      axiosConfigDefaults: {
        baseURL: server,
        validateStatus: () => true,
        headers: {
          'X-API-Key': token,
        },
      },
    })
    this.client = api.initSync<MotionApiClient>()
  }

  /**
   * Register bindings to the container
   */
  register() {
    this.app.container.bind('motion', () => {
      return this.client
    })
  }

  /**
   * The container bindings have booted
   */
  async boot() {}

  /**
   * The application has been booted
   */
  async start() {}

  /**
   * The process has been started
   */
  async ready() {}

  /**
   * Preparing to shutdown the app
   */
  async shutdown() {}
}
