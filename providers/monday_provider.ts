import type { ApplicationService } from '@adonisjs/core/types'
import { token, version } from '#config/monday'
import mondaySdk from 'monday-sdk-js'
import { query as graphql } from 'gql-query-builder'
import type { MondayClientSdk } from 'monday-sdk-js/types/client-sdk.interface.js'

declare type NestedField = {
  operation: string
  variables: QueryBuilderOptions[]
  fields: Fields
  fragment?: boolean | null
}

declare type Fields = Array<string | object | NestedField>

interface Operation {
  name: string
  alias: string
}

declare type VariableOptions =
  | {
      type?: string
      name?: string
      value: any
      list?: boolean | [boolean]
      required?: boolean
    }
  | {
      [k: string]: any
    }

interface QueryBuilderOptions {
  operation: string | Operation
  fields?: Fields
  variables?: VariableOptions
}

export interface MondayApiQueryFn {
  (
    operation: QueryBuilderOptions['operation'],
    fields?: QueryBuilderOptions['fields'],
    variables?: QueryBuilderOptions['variables']
  ): Promise<any>
}

declare module '@adonisjs/core/types' {
  interface ContainerBindings {
    'monday': MondayClientSdk
    'monday.api': MondayClientSdk['api']
    'monday.api.query': MondayApiQueryFn
    'monday.listen': MondayClientSdk['listen']
    'monday.get': MondayClientSdk['get']
    'monday.execute': MondayClientSdk['execute']
    'monday.storage': MondayClientSdk['storage']
  }
}

export default class MondayProvider {
  private client: MondayClientSdk
  constructor(protected app: ApplicationService) {
    this.client = mondaySdk()
    this.client.setToken(token)
    this.client.setApiVersion(version)
  }

  /**
   * Register bindings to the container
   */
  register() {
    this.app.container.bind('monday', () => {
      return this.client
    })
    this.app.container.bind('monday.api', () => {
      return this.client.api
    })
    this.app.container.bind('monday.api.query', () => {
      const fn: MondayApiQueryFn = async (operation, fields, variables) => {
        const { query: qq, variables: qv } = graphql({ operation, fields, variables })
        return this.client.api(qq, qv)
      }
      return fn
    })
    this.app.container.bind('monday.listen', () => {
      return this.client.listen
    })
    this.app.container.bind('monday.get', () => {
      return this.client.get
    })
    this.app.container.bind('monday.execute', () => {
      return this.client.execute
    })
    this.app.container.bind('monday.storage', () => {
      return this.client.storage
    })
  }

  /**
   * The container bindings have booted
   */
  async boot() {}

  /**
   * The application has been booted
   */
  async start() {}

  /**
   * The process has been started
   */
  async ready() {}

  /**
   * Preparing to shutdown the app
   */
  async shutdown() {}
}
