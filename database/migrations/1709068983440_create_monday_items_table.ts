import { BaseSchema } from '@adonisjs/lucid/schema'

/**
 * Model which represents a Monday item
 * See @link https://developer.monday.com/api-reference/reference/items for more information
 */

export default class extends BaseSchema {
  protected tableName = 'monday_items'

  async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.integer('monday_board_id').notNullable().unsigned()
      table.integer('monday_creator_id').notNullable().unsigned()
      table.integer('monday_item_id').notNullable().unsigned()
      table.integer('monday_items_parent_id').unsigned().nullable()
      table.string('monday_email').notNullable()
      table.string('monday_name').notNullable()
      table.string('monday_state').notNullable()
      table.timestamp('monday_created_at').notNullable()
      table.timestamp('created_at')
      table.timestamp('updated_at')
    })
  }

  async down() {
    this.schema.dropTable(this.tableName)
  }
}
