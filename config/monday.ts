import env from '#start/env'

export const token: string = env.get('MONDAY_API_TOKEN', '')
export const version: string = env.get('MONDAY_API_VERSION', '2023-10')
