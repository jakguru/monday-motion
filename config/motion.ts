import env from '#start/env'

export const server: string = env.get('MOTION_API_SERVER', '')
export const token: string = env.get('MOTION_API_TOKEN', '')
