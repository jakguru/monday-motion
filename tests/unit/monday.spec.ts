import { test } from '@japa/runner'
import app from '@adonisjs/core/services/app'

test.group('Monday', () => {
  test('able to make authenticated api requests', async ({ assert }) => {
    const mondayApiQueryFn = await app.container.make('monday.api.query')
    const result = await mondayApiQueryFn('me', ['id', 'name'])
    assert.exists(result)
    assert.exists(result.data)
    assert.exists(result.data.me)
  })
})
