import { test } from '@japa/runner'
import app from '@adonisjs/core/services/app'

test.group('Motion', () => {
  test('able to make authenticated api requests', async ({ assert }) => {
    const client = await app.container.make('motion')
    const { data: me } = await client.UsersController_getMe()
    assert.exists(me)
  })
})
